#pragma begin_clear_text
# *****************************************************************************
#	DBD::Teradata::Cli - CLI Wrapper for DBD::Teradata
#    Copyright (C) 2004-2009, Presicient Corp., USA
#    email: info@presicient.com
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; Refer to the included LICENSE file, or the LICENSE section 
#    of the DBD::Teradata documentation, for details. If neither is available,
#    write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#	provides general purpose wrappers for buffer mode CLI operations
#
#	History:
#		2004-Jul-23		D. Arnold		Coded
#
#		2006-Sep-08		D. Arnold		Convert to pure XS
# *****************************************************************************
#pragma end_clear_text

package DBD::Teradata::Cli;
use Time::HiRes qw(time sleep);
use DBD::Teradata qw(:tdat_parcels);
use Exporter;

BEGIN {
	our @ISA = qw(Exporter);
#pragma begin_redact
use constant  EM_OK => 0;			# no error
use constant  EM_VERSION => 202;	# wrong version
use constant  EM_BUFSIZE => 203;	# bad buf size
use constant  EM_MOSI => 204;		# MOSI error routine
use constant  EM_FUNC => 205;		# bad mtdp function call
use constant  EM_ASSIGN => 206;		# err parcel during assign
use constant  EM_NETCONN => 207;	# err doing network connect
use constant  EM_NOTIDLE => 208;	# not ready for new req
use constant  EM_REQID => 209;		# request id unknown
use constant  EM_NOTACTIVE => 210;	# no START/CONT
use constant  EM_NODATA => 211;		# no data
use constant  EM_DATAHERE => 212;	# data ready
use constant  EM_ERRPARCEL => 213;	# got error parcel
use constant  EM_NOSCS => 214;		# SCS not found in mtdp
use constant  EM_CONNECT => 215;	# error pcl during connect
use constant  EM_BUFOVERFLOW => 216;	# rsp buffer too small
use constant  EM_TIMEOUT => 217;	# Time out
use constant  EM_BREAK => 218;		# Break signal
use constant  EM_DBC_CRASH_B => 219;	# CRASHED before request
use constant  EM_DBC_CRASH_A => 220;	# CRASHED after request

use constant CLI_NOMEM => 300;		# out of memory
use constant CLI_SESSOVER => 301; 	# exceeded max no of sessions
use constant CLI_BADBUFRQ => 302; 	# invalid buffer size
use constant CLI_BADLOGON => 303; 	# invalid logon string
use constant CLI_NOSESSION => 304;	# specified session doesn't exist
use constant CLI_NOREQUEST => 305;	# specified req does not  exist
use constant CLI_BADPARCEL => 306;  # invalid parcel received
use constant CLI_REQEXHAUST => 307;	# request data exhausted
use constant CLI_BUFOVFLOW => 308;	# parcel data too large for req buffer
use constant CLI_REWINDING => 309;  # rewinding request
use constant CLI_NOREW => 310;		# cannot rewind
use constant CLI_NOACTIVE => 311;	# no active sessions
use constant CLI_INVALIDFN => 312;	# wrong function code
use constant CLI_BADID => 313;		# bad logon string
use constant CLI_NOPASSWORD => 314;	# no password
use constant CLI_SESSCRASHED => 315;# session crashed
use constant CLI_NOTINIT => 316; 	# NOT initialized
use constant CLI_BADENVIRON => 317;	# invalid environment variable
use constant CLI_NOSPB => 318;		# no SPB file found
use constant CLI_BADSPB => 319;		# SPB file has bad format
use constant CLI_BADRECON => 320;	# Bad reconnect options
use constant CLI_BADLEN => 321;		# Bad dbcarea length
use constant CLI_BADOPTS => 322;	# Bad fvar/floc options
use constant CLI_BADRSUP => 323;	# using data !permitted in RSUP
use constant CLI_BADIDAT => 324;	# bad indicdata option DCR4278
use constant CLI_BADREQMODE => 325;	# bad req mode  option DCR4278
use constant CLI_NOSESSNO => 333;	# DBC session id not supplied

use constant CLI_REQOVFLOW => 350;  # Request size exceeds maximum.
use constant CLI_SQLUSREXT => 351;  # User SQL exit
use constant CLI_BADTXSEM => 352;	# Bad Trans. Semantics
use constant CLI_BADLANCONF => 353;	# Unrecognized Language Conf. option.
use constant CLI_NOQINFO => 354;	# query information is unavailable.
use constant CLI_QEPITEM => 355;	# Requested item in DBCHQEP is invalid.
use constant CLI_QEPALEN => 356;	# Return area too small.
use constant CLI_QEPAREA => 357;	# No return area supplied in DBCHQEP.
use constant CLI_CANCELLOGON => 370;  # cancel logon.
use constant CLI_NOBUFFER => 376;	# No buffer provided for request mode 'B'
use constant CLI_BADUEFUNC => 377;  # Invalid function number
use constant CLI_BADUECNTA => 378;  # Invalid context area pointer
use constant CLI_BADUEKIND => 379;  # Invalid event kind
use constant CLI_BADUEDATA => 380;  # Invalid data
use constant CLI_NOACTIVEREQUEST => 385;  # No active request found
use constant CLI_BADMODE => 386;
use constant CLI_BADMOVEMODE => 387;
use constant CLI_BADPROCOPT => 388;
use constant CLI_NOMULTIPART => 389;	# Multipart response mode not supported by the server
use constant CLI_BADRETURNOBJ => 390;	# Unrecognized value for Return-object option
use constant CLI_BADCONTDATA => 391;	# Unrecognized value for Continued-data option
use constant CLI_INCONTDATA => 392;		# Inconsistent Continued-data option
use constant CLI_NOALTSUPPORT => 393;	# No APH support at the server

use constant TDCLI_DBC_CTXT => 1;
use constant TDCLI_ACTIVE => 2;
use constant TDCLI_LAST_RESP => 3;
use constant TDCLI_DEBUG => 4;
use constant TDCLI_SESSNO => 5;
#pragma end_redact

our @EXPORT = ();
our @EXPORT_OK = ();

our %EXPORT_TAGS = (
	tdcli_errors => [
#pragma begin_redact
		qw/EM_OK EM_VERSION EM_BUFSIZE EM_MOSI EM_FUNC EM_ASSIGN EM_NETCONN
		EM_NOTIDLE EM_REQID EM_NOTACTIVE EM_NODATA EM_DATAHERE EM_ERRPARCEL
		EM_NOSCS EM_CONNECT EM_BUFOVERFLOW EM_TIMEOUT EM_BREAK EM_DBC_CRASH_B
		EM_DBC_CRASH_A
		CLI_NOMEM CLI_SESSOVER CLI_BADBUFRQ CLI_BADLOGON CLI_NOSESSION CLI_NOREQUEST
		CLI_BADPARCEL CLI_REQEXHAUST CLI_BUFOVFLOW CLI_REWINDING CLI_NOREW
		CLI_NOACTIVE CLI_INVALIDFN CLI_BADID CLI_NOPASSWORD CLI_SESSCRASHED
		CLI_NOTINIT CLI_BADENVIRON CLI_NOSPB CLI_BADSPB CLI_BADRECON CLI_BADLEN
		CLI_BADOPTS CLI_BADRSUP CLI_BADIDAT CLI_BADREQMODE CLI_NOSESSNO
		CLI_REQOVFLOW CLI_SQLUSREXT CLI_BADTXSEM CLI_BADLANCONF CLI_NOQINFO
		CLI_QEPITEM CLI_QEPALEN CLI_QEPAREA CLI_CANCELLOGON CLI_NOBUFFER
		CLI_BADUEFUNC CLI_BADUECNTA CLI_BADUEKIND CLI_BADUEDATA CLI_NOACTIVEREQUEST
		CLI_BADMODE CLI_BADMOVEMODE CLI_BADPROCOPT CLI_NOMULTIPART
		CLI_BADRETURNOBJ CLI_BADCONTDATA CLI_INCONTDATA CLI_NOALTSUPPORT
		/
#pragma end_redact
	],
);
#pragma end_clear_text

Exporter::export_tags(keys %EXPORT_TAGS);

}

our $VERSION = '12.001';

use strict;
use warnings;
#
#	we have to perist this list of contexts soley to clean up
#	on CLONE
#
our %dbc_ctxts = ();

sub CLONE {
	map { $_->[TDCLI_DBC_CTXT] = 0; } values %dbc_ctxts;
	%dbc_ctxts = ();
}

sub new {
	my ($class, $logonstr, $mode, $partition, $lsn, $logonsrc, $charset, $debug) = @_;
	my $obj = [ ];
	bless $obj, $class;

	my ($errno, $errstr, $dbc, $hostid, $sessno, $version) =
		(0, '', undef, undef, undef, undef);

	print "cli_connect: initing\n"
		if $debug;
#
#	$dbc contains the pointer to the XS context area
#
	$dbc = tdxs_init_dbcarea($debug);
	return (undef, -1, "Cannot init dbcarea")
		unless $dbc;

	$obj->[TDCLI_DEBUG] = $debug;

	print "cli_connect: connecting $logonstr\n"
		if $debug;

	$partition = 'DBC/SQL         ' unless $partition;
	my $lsnfunc = defined($lsn) ? ($lsn ? 2 : 1) : 0;
	$lsn = 0 unless defined($lsn);
	my $runstring = pack('A16 L S S',
		$partition,
		$lsn,
		$lsnfunc, 0);

	($sessno, $hostid, $version, $lsn, $errno, $errstr) =
		tdxs_get_connection($dbc, $logonstr, $mode, $runstring, $logonsrc, $charset);

	unless ($errno) {
		$obj->[TDCLI_DBC_CTXT] = $dbc;
		print "cli_connect: connected\n" if $debug;
		$obj->[TDCLI_SESSNO] = $sessno;
		$dbc_ctxts{"$sessno\_$$"} = $obj;
		return ($obj, undef, undef, $hostid, $sessno, $version, $lsn);
	}

	print "cli_connect: didn't connect: $errstr\n"
		if $debug;

	return ($obj, $errno, $errstr, undef, undef, undef, undef);
}

sub cli_get_tdat_release {
	return tdxs_get_tdat_release($_[0]->[TDCLI_DBC_CTXT]);
}

sub cli_set_debug {
	tdxs_set_debug($_[0]->[TDCLI_DBC_CTXT], $_[1]);
	$_[0]->[TDCLI_DEBUG] = $_[1];
}

sub cli_disconnect {
	my ($err, $errstr);
	$err = tdxs_cleanup($_[0]->[TDCLI_DBC_CTXT], $errstr)
		if $_[0]->[TDCLI_DBC_CTXT];
	$_[0]->[TDCLI_LAST_RESP] = undef;
	$_[0]->[TDCLI_DBC_CTXT] = undef;
	delete $dbc_ctxts{$_[0]->[TDCLI_SESSNO] . "_$$"};
	1;
}

sub cli_test_leak {
	my $sql = 'sel user,date,time' . (' ' x 4000);
	my $req = pack('a* SSa10 SS a* SS S',
		"\0" x 52,	# faux lan header
		PclOPTIONS, 14, 'RE',
		PclREQUEST, 4 + length($sql), $sql,
		PclRESP, 4 + 2, 64000);

	tdxs_test_leak($_[0]->[TDCLI_DBC_CTXT], $req, $_[1]);
}

sub cli_send_request {
	my $obj = shift;
#
#	len in $_[0], keepresp in $_[1], buffer in $_[3], resp_size in $_[4]
#
	print "cli_send_request: sending\n"
		if $obj->[TDCLI_DEBUG];

	my ($reqid, $err, $errstr) = tdxs_send_request($obj->[TDCLI_DBC_CTXT], @_);

	print "cli_send_request: request $reqid sent\n"
		unless $err || (! $obj->[TDCLI_DEBUG]);
#
#	save the active request id
#
	$obj->[TDCLI_ACTIVE] = $reqid unless $err;

	return ($reqid, $err, $errstr);
}
#
#	since CLI is a stinking pile of dogshit,
#	we'll need this...and have to keep a fucking
#	bunch of context around...to do polling
#
sub cli_has_response {
	my ($obj, $wait) = @_;

	print "cli_has_response: checking for response\n"
		if $obj->[TDCLI_DEBUG];
	my $buffer;
#
#	exit if we're not active
#
	unless (defined($obj->[TDCLI_ACTIVE])) {
		print "cli_has_response: session not active\n"
			if $obj->[TDCLI_DEBUG];
		return undef;
	}
#
#	if we had something left around, return it
#
	if (defined($obj->[TDCLI_LAST_RESP])) {
		print "cli_has_response: return old response for $obj->[TDCLI_ACTIVE]\n"
			if $obj->[TDCLI_DEBUG];
		return $obj->[TDCLI_LAST_RESP]->[3];
	}
#
#	we may spin our wheels here; if we have a pending response,
#	then cli_get_response will return and discard it
#	and we then re-instantiate it
#
	my $reqid = $obj->[TDCLI_ACTIVE];
	my ($err, $errstr) = $obj->cli_get_response(\$buffer, $reqid, undef, $wait);

 	if ($err && ($err == EM_NODATA)) {
		print "cli_has_response: no response for $reqid\n"
			if $obj->[TDCLI_DEBUG];
		return undef;
	}

	print "cli_has_response: got a response for $reqid\n"
		if $obj->[TDCLI_DEBUG];
	$obj->[TDCLI_LAST_RESP] = [ $err, $errstr, $buffer, $reqid ];
	return $reqid;
}
#
#	get a response, possibly waiting
#	up to $wait seconds (can be fractional)
#	for a response
#	if $wait is less than 0, then
#		wait forever
#	elsif $wait is 0 or undefined, then just poll
#	else poll for a result, and loop until
#		wait interval expires
#	note- WINCLI may not handle this properly
#
sub cli_get_response {
	my ($obj, $buffer, $reqid, $keepresp, $wait) = @_;

	my ($err, $errstr);
	my $debug = $obj->[TDCLI_DEBUG];

	print 'cli_get_response: getting ', ($keepresp ? 'KEEPRESP' : 'RESP'), " response for $reqid\n"
		if $debug;

	return (EM_NOTIDLE, 'EM_NOTIDLE: No data received from dbc.')
		if ($obj->[TDCLI_ACTIVE] && ($obj->[TDCLI_ACTIVE] != $reqid));

	if ($obj->[TDCLI_LAST_RESP] &&
		($obj->[TDCLI_LAST_RESP]->[3] == $reqid)) {
#
#	if we had a response, return it
#	note that this is a one-shot
#	should we keep a hash of these ???
#
		print "cli_get_response: returning stored response\n"
			if $debug;
		($err, $errstr, $$buffer, $reqid) = @{$obj->[TDCLI_LAST_RESP]};
		$obj->[TDCLI_LAST_RESP] = undef;
		return ($err, $errstr);
	}

	print "cli_get_response: getting response\n"
		if $debug;
#
#	get rid of any old response buffer before creating a new one
#
	unless ($wait) {
		($err, $errstr) = tdxs_get_response($obj->[TDCLI_DBC_CTXT], $$buffer, $reqid, $keepresp, $wait);
#
#	to handle CONTINUE cases, we have to keep an activation here
#
		$obj->[TDCLI_ACTIVE] = $reqid
			if ($err && ($err == EM_NODATA));

		print "cli_get_response: got response length ", length($$buffer), "\n"
			unless ($err || (! $debug));

#		$$buffer = $tbuf;
		return ($err, $errstr);
	}

	my $started = time();

	while (($wait < 0) || ((time() - $started) < $wait)) {
		($err, $errstr) = tdxs_get_response($obj->[TDCLI_DBC_CTXT], $$buffer, $reqid, $keepresp, ($wait < 0));
		last unless (($err == EM_NODATA) || ($err == EM_NOTIDLE));
#		sleep 0.05;	#	wait a tick
	}

	$obj->[TDCLI_ACTIVE] = $reqid
		if ($err && ($err == EM_NODATA));

	print "cli_get_response: got response length ", length($$buffer), "\n"
		unless ($err || (! $debug));

#	$$buffer = $tbuf;
	return ($err, $errstr);
}

sub cli_end_request {
	my ($obj, $reqid) = @_;

	print "cli_end_request: Ending request $reqid\n"
		if $obj->[TDCLI_DEBUG];

	my ($err, $errstr) = tdxs_end_request($obj->[TDCLI_DBC_CTXT], $reqid);
#
#	suppress NOREQUEST errors
#
	($err, $errstr) = (0, undef)
		if ($err == CLI_NOREQUEST);

	print "end_request failed: $err $errstr\n"
		if ($obj->[TDCLI_DEBUG] && $err);

	$obj->[TDCLI_LAST_RESP] = undef
		if ($obj->[TDCLI_LAST_RESP] &&
			($obj->[TDCLI_LAST_RESP]->[3] == $reqid));
	$obj->[TDCLI_ACTIVE] = undef
		if ($obj->[TDCLI_ACTIVE] &&
			($obj->[TDCLI_ACTIVE] == $reqid));
	return ($err, $errstr)
}

sub cli_abort_request {
	print "cli_abort_request: Aborting request $_[1]\n"
		if $_[0]->[TDCLI_DEBUG];

	return tdxs_abort_request($_[0]->[TDCLI_DBC_CTXT], $_[1]);
}
#
#	class method to wait on list of CLI contexts
#
sub cli_wait_for_response {
	my $timeout = shift;
#
#	arglist is set of 4-tuples of [ ID, CLI object, $reqid, $keepresp ]
#	we test each tuple for a pending response; if ready, we flag the tuple
#	as done by setting ID to zero, after we push the ID on the ready array
#	After testing each tuple, if any are ready, we return; else, we retry
#	until the timeout is exhausted
#
	my $started = time();
	my @ready = ();
	my ($id, $obj, $reqid, $keepresp);
	while ((!defined($timeout)) || ($timeout > (time() - $started))) {
		my $i = 0;
		while ($i < scalar @_) {
#			$i += 4, next
#				unless defined($_[$i]);

			my $buffer; #  = \$bufs[$i >> 2];
			($id, $obj, $reqid, $keepresp) = @_[$i .. $i + 3];
			my ($err, $errstr) =
				tdxs_get_response($obj->[TDCLI_DBC_CTXT], $buffer, $reqid, $keepresp, undef);

	 		next
	 			if ($err && ($err == EM_NODATA));

			print 'cli_has_response: got a response for ', $reqid, "\n"
				if $obj->[TDCLI_DEBUG];
			push @ready, $id;
#			$_[$i] = undef;	# mark ID
			$obj->[TDCLI_LAST_RESP] = [ $err, $errstr, $buffer, $reqid ];
			$i += 4;
		}
		return @ready if scalar @ready;
		select(undef, undef, undef, 0.05);	# wait a tick
	}
	return ();
}

sub DESTROY {
	my ($err, $errstr);
#
#	can only destroy in our thread/process
#
	return 1 unless $dbc_ctxts{$_[0]->[TDCLI_SESSNO] . "_$$"};

	$err = tdxs_cleanup($_[0]->[TDCLI_DBC_CTXT], $errstr)
		if $_[0]->[TDCLI_DBC_CTXT];
	$_[0]->[TDCLI_LAST_RESP] = undef;
	$_[0]->[TDCLI_DBC_CTXT] = undef;
	delete $dbc_ctxts{$_[0]->[TDCLI_SESSNO] . "_$$"};
}

#
#	bootstrap the XS
#
require XSLoader;
XSLoader::load('DBD::Teradata::Cli', $VERSION);

1;

